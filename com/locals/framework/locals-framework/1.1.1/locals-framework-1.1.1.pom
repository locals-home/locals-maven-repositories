<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.locals.framework</groupId>
    <artifactId>locals-framework</artifactId>
    <packaging>pom</packaging>
    <version>1.1.1</version>
    <modules>
        <module>framework-common</module>
        <module>framework-security</module>
        <module>framework-microservice</module>
        <module>framework-web</module>
        <module>framework-core</module>
        <module>framework-datasource</module>
        <module>framework-elasticsearch</module>
        <module>framework-datasource-multiple</module>
        <module>framework-datasource-plus</module>
        <module>framework-datasource-support</module>
        <module>framework-aliyun-oss</module>
        <module>framework-security-support</module>
        <module>framework-mongodb</module>
        <module>framework-es</module>
        <module>framework-event</module>
    </modules>

    <repositories>
        <repository>
            <id>alimaven</id>
            <name>aliyun maven</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
        </repository>

        <!-- 码云私仓 -->
        <repository>
            <id>locals-maven-repositories</id>
            <url>https://gitee.com/locals-home/locals-maven-repositories/raw/master</url>
        </repository>
    </repositories>

    <distributionManagement>

        <repository>
            <id>localRepository</id>
            <!-- 本地的 locals-maven-repositories 目录，deploy时会打包到此目录-->
            <url>file:../locals-maven-repositories</url>
        </repository>

    </distributionManagement>

    <properties>
        <!-- framework -->
        <framework.common.version>1.1.7</framework.common.version>
        <framework.core.version>1.1.1</framework.core.version>
        <framework.datasource.version>1.1.1</framework.datasource.version>
        <framework.datasource.multiple.version>1.1.1</framework.datasource.multiple.version>
        <framework.datasource.plus.version>1.1.3</framework.datasource.plus.version>
        <framework.datasource.support.version>1.1.1</framework.datasource.support.version>
        <framework.elasticsearch.version>1.1.1</framework.elasticsearch.version>
        <framework.message.version>1.1.1</framework.message.version>
        <framework.web.version>1.1.4</framework.web.version>
        <framework.microservice.provider.version>1.1.5</framework.microservice.provider.version>
        <framework.security.version>1.1.1</framework.security.version>
        <framework.security.support.version>1.1.8</framework.security.support.version>
        <framework.aliyun.oss.version>1.1.1</framework.aliyun.oss.version>
        <framework.mongodb.version>1.1.1</framework.mongodb.version>

        <!-- ################################################# -->
        <!-- ################################################# -->
        <!-- ################################################# -->
        <!-- ################################################# -->

        <!-- ================================================= -->
        <!-- 3rd -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.8</java.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <pagehelper.version>4.1.3</pagehelper.version>
        <mybatis-spring-boot-starter.version>1.3.0</mybatis-spring-boot-starter.version>
        <mybatisplus-spring-boot-starter.version>1.0.5</mybatisplus-spring-boot-starter.version>
        <mybatis-plus.version>2.1.9</mybatis-plus.version>
        <jjwt.version>0.7.0</jjwt.version>
        <lombok.version>1.16.16</lombok.version>
        <assertj-core.version>3.8.0</assertj-core.version>
        <rxjava.version>2.1.3</rxjava.version>
        <elasticsearch.version>5.5.2</elasticsearch.version>
        <spring-boot.version>1.5.6.RELEASE</spring-boot.version>
        <hutool.version>3.1.1</hutool.version>
        <hutool4.version>4.1.5</hutool4.version>
        <guava.version>21.0</guava.version>
        <druid.version>1.1.8</druid.version>
        <poi.version>3.17</poi.version>
        <!-- ================================================= -->


        <!-- entity -->
        <locals-entity.version>1.2.0</locals-entity.version>
    </properties>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.6.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>


    <dependencyManagement>
        <dependencies>
            <!-- ################################################# -->
            <!-- framework -->
            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-common</artifactId>
                <version>${framework.common.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-datasource</artifactId>
                <version>${framework.datasource.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-datasource-multiple</artifactId>
                <version>${framework.datasource.multiple.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-datasource-plus</artifactId>
                <version>${framework.datasource.plus.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-datasource-support</artifactId>
                <version>${framework.datasource.support.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-elasticsearch</artifactId>
                <version>${framework.elasticsearch.version}</version>
            </dependency>


            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-security</artifactId>
                <version>${framework.security.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-security-support</artifactId>
                <version>${framework.security.support.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-core</artifactId>
                <version>${framework.core.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>microservice-provider</artifactId>
                <version>${framework.microservice.provider.version}</version>
            </dependency>


            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-web</artifactId>
                <version>${framework.web.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-mongodb</artifactId>
                <version>${framework.mongodb.version}</version>
            </dependency>


            <!-- ################################################# -->
            <!-- ################################################# -->
            <!-- ################################################# -->
            <!-- ################################################# -->

            <!-- 3rd -->
            <dependency>
                <groupId>com.github.pagehelper</groupId>
                <artifactId>pagehelper</artifactId>
                <version>${pagehelper.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis-spring-boot-starter.version}</version>
            </dependency>

            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatisplus-spring-boot-starter</artifactId>
                <version>${mybatisplus-spring-boot-starter.version}</version>
            </dependency>

            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>

            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt</artifactId>
                <version>${jjwt.version}</version>
            </dependency>

            <dependency>
                <groupId>io.reactivex.rxjava2</groupId>
                <artifactId>rxjava</artifactId>
                <version>${rxjava.version}</version>
            </dependency>


            <dependency>
                <groupId>org.elasticsearch</groupId>
                <artifactId>elasticsearch</artifactId>
                <version>${elasticsearch.version}</version>
            </dependency>

            <dependency>
                <groupId>org.elasticsearch.client</groupId>
                <artifactId>transport</artifactId>
                <version>${elasticsearch.version}</version>
            </dependency>

            <dependency>
                <artifactId>servo-core</artifactId>
                <groupId>com.netflix.servo</groupId>
                <version>0.10.1</version>
            </dependency>

            <dependency>
                <groupId>com.xiaoleilu</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>

            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool4.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid</artifactId>
                <version>${druid.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.poi</groupId>
                <artifactId>poi-ooxml</artifactId>
                <version>${poi.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.entity</groupId>
                <artifactId>locals-entity</artifactId>
                <version>${locals-entity.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <!-- use 2.8.0 for Java 7 projects -->
            <version>${assertj-core.version}</version>
            <scope>test</scope>
        </dependency>


    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skipTests>true</skipTests>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>